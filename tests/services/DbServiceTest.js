var YUITest = require("yuitest");
var instrumentedDir = "../../build/instrumented";
var fs = require('fs');

// turn off the instrumented code if it is unavailable
if(!fs.existsSync("build/instrumented")) {
    instrumentedDir = "../..";
}

// requires
var DbService = require(instrumentedDir + "/services/DbService");

YUITest.TestRunner.add(new YUITest.TestCase({
    name: "DbServiceTest",

    createDatabase: function(success) {
        var dbName = this.getRandomName(8);

        DbService.CreateDatabase({
            name: dbName,
            success: success,
            fail: function() {
                YUITest.Assert.fail("create database did not succeed.");
            }
        });

        return dbName;
    },

    deleteDatabase: function(name) {
        fs.unlinkSync("db/" + name + ".sqlite3");
    },

    getRandomName: function(len) {
        while(true) {
            var r = Math.floor(Math.random() * Math.pow(16, len)).toString(16);
            if(r.length == 8) {
                return r;
            }
        }
    },

    testValidDatabaseNames: function() {
        YUITest.Assert.isTrue(DbService.ValidateDatabaseName('abcde'));
        YUITest.Assert.isFalse(DbService.ValidateDatabaseName('ab-cde'));
        YUITest.Assert.isTrue(DbService.ValidateDatabaseName('ABC'));
        YUITest.Assert.isTrue(DbService.ValidateDatabaseName('AbC123xy'));
    },
    
    testCreateDatabase: function() {
        var self = this;

        var dbName = this.createDatabase(function(result) {
            self.resume(function() {
                YUITest.Assert.isObject(result);
                YUITest.Assert.areEqual(dbName, result.name);

                // make sure the file now exists on disk
                YUITest.Assert.isTrue(fs.existsSync("db/" + dbName + ".sqlite3"));

                // clean up
                this.deleteDatabase(dbName);
            })
        });

        this.wait(1000);
    },

    testCreateTable: function() {
        var self = this;
        var tableName = 'MyTable';

        this.createDatabase(function(result) {
            self.resume(function() {
                result.createTable({
                    name: tableName,
                    dbname: result.name,
                    success: function() {
                        self.resume(function() {
                            YUITest.Assert.isTrue(true);

                            // clean up
                            this.deleteDatabase(result.name);
                        });
                    },
                    fail: function(err) {
                        self.resume(function() {
                            YUITest.Assert.fail(err);
                        });
                    }
                });

                this.wait(1000);
            });
        });

        this.wait(1000);
    }

}));

if(fs.existsSync("build/instrumented")) {
	console.log(YUITest.TestRunner.getCoverage(YUITest.CoverageFormat.JSON));
}

