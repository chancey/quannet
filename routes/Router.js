var RootController = require('../controllers/RootController');
var ApiController = require('../controllers/ApiController');

/**
 * Handles routing.
 *
 * @namespace routes
 * @class Router
 */
exports.router = function(app) {
    // index controller
    app.get('/', RootController.View);
    app.get('/:dbname([a-z]+)/', RootController.View);
    
    // no qualifiers
    app.get('/api/createdb', ApiController.DatabaseCreate);

    // by database
    app.get('/api/[0-9a-f]{8,}/describe', ApiController.DatabaseDescribe);
    app.get('/api/[0-9a-f]{8,}/tables', ApiController.DatabaseTables);

    // by table
    app.get('/api/[0-9a-f]{8,}\-[0-9a-f]{4,}/describe', ApiController.TableDescribe);
};
