var DbCoreService = require('./DbCoreService');

/**
 * Database service.
 *
 * @namespace services
 * @class DbService
 * @constructor
 * @param [dbName] {String} Provided database name.
 */
function DbService(dbName) {
}

/**
 * Validate a database name. Database names can only contain lower/upper case letters and numbers but must
 * start with a letter.
 *
 * @method ValidateDatabaseName
 * @param dbName {String} The database name to test.
 * @return {Boolean} true if the database name is valid, otherwise false.
 * @static
 */
DbService.ValidateDatabaseName = function(dbName) {
    return dbName.match(/^[a-zA-Z][a-zA-Z0-9]+$/) != null;
}

/**
 * Create a database.
 *
 * @method CreateDatabase
 * @param meta {Object}
 * @param meta.name {String} The name of the database, this will be validated against
 * DbService.validateDatabaseName
 * @param [meta.success] {Function} Callback function() to run on success.
 * @param [meta.fail] {Function} Callback function() to run on failure.
 * @static
 */
DbService.CreateDatabase = function(meta) {
    DbCoreService.CreateDatabase({
        name: meta.name,
        success: meta.success,
        fail: meta.fail
    });
}

/**
 * Create a table.
 *
 * @method createTable
 * @param meta {Object}
 * @param meta.name {String} The name of the table
 * @param [meta.success] {Function} Callback function() to run on success.
 * @param [meta.fail] {Function} Callback function() to run on failure.
 */
DbService.prototype.createTable = function(meta) {
    // prepare callbacks
    meta.success = (meta.success ? meta.success : function() {});
    meta.fail = (meta.fail ? meta.fail : function() {});

    // fill in database name
    meta.dbname = dbName;

    // execute
    console.log(meta);
    DbCoreService.createTable({

    });
    /*db.run("CREATE TABLE lorem (info TEXT)", function() {
                var stmt = db.prepare("INSERT INTO lorem VALUES (?)");
                for (var i = 0; i < 10; i++) {
                    stmt.run("Ipsum " + i);
                }

                stmt.finalize(function() {
                    db.all("SELECT rowid AS id, info FROM lorem", function(err, rows) {
                        if(err) {
                            meta.fail();
                            return;
                        }

                        rows.forEach(function (row) {
                            console.log(row.id + ": " + row.info);
                        });
                        db.close(function() {
                            if(meta.success) {
                                meta.success({
                                    'id': 'abcde'
                                });
                            }
                            return;
                        });
                    });
                });
            }*/
}

/**
 * Describe a database.
 *
 * @method describe
 */
DbService.prototype.describe = function() {
    var resObject = {
        registered: true,
        dbname: dbName,
        description: 'my cool description'
    };
    
    return resObject;
};

module.exports = DbService;
