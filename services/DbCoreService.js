var sqlite3 = require('sqlite3');

/**
 * Core database.
 *
 * @namespace services
 * @class DbCoreService
 * @param meta {Object} Attributes.
 * @param meta.name {String} The database name.
 * @param meta.handle {sqlite3} Database handle.
 * @param meta.handle.open {Boolean} If the handle is currently active.
 * @param meta.handle.filename {String} The relative file path on disk.
 * @param meta.handle.mode {Integer} Disk permissions.
 */
function DbCoreService(meta) {
	this.name = meta.name;
	this.handle = meta.handle;
}

/**
 * To cache database connections (effectivly db handles in sqlite3) used by DbCoreService.OpenDatabase()
 * @property DbHandles {Object} 
 * @static
 */
DbCoreService.DbHandles = {};

/**
 * Open a database connection (handle).
 *
 * @namespace services
 * @method OpenDatabase
 * @param meta {Object} Attributes.
 * @param meta.name {String} The name of the database to open.
 * @param [meta.success] {Function} Callback function(db) to run on success.
 * @param [meta.fail] {Function} Callback function() to run on failure.
 * @return void
 * @static
 */
DbCoreService.OpenDatabase = function(meta) {
    // prepare callbacks
    meta.success = (meta.success ? meta.success : function(db) {});
    meta.fail = (meta.fail ? meta.fail : function() {});

    // is the handle already open?
	if(this.DbHandles[meta.name]) {
		console.log('CACHED!');
		meta.success(this.DbHandles[meta.name]);
	}
	else {
		var db = new sqlite3.Database('db/' + meta.name + '.sqlite3', function() {
			var p = { name: meta.name, handle: db };
			return meta.success(new DbCoreService(p));
		}, meta.fail);
		this.DbHandles[meta.name] = db;
	}
}

/**
 * Create a database.
 *
 * @namespace services
 * @method CreateDatabase
 * @param meta {Object} Attributes.
 * @param meta.name {String} The name of the database to create.
 * @param [meta.success] {Function} Callback function(db) to run on success.
 * @param [meta.fail] {Function} Callback function() to run on failure.
 * @return void
 * @static
 */
DbCoreService.CreateDatabase = function(meta) {
    // prepare callbacks
    meta.success = (meta.success ? meta.success : function(meta) {});
    meta.fail = (meta.fail ? meta.fail : function() {});

    // open the database
	this.OpenDatabase({
		name: meta.name,
		success: meta.success,
		fail: meta.fail
	});
}

/**
 * Create a table.
 *
 * @namespace services
 * @method createTable
 * @param meta {Object} Attributes.
 * @param meta.name {String} The name of the table to create.
 * @param meta.dbname {String} The handle of the database.
 * @param [meta.success] {Function} Callback function(db) to run on success.
 * @param [meta.fail] {Function} Callback function() to run on failure.
 * @return void
 */
DbCoreService.prototype.createTable = function(meta) {
    // prepare callbacks
    meta.success = (meta.success ? meta.success : function(meta) {});
    meta.fail = (meta.fail ? meta.fail : function() {});

	// build the SQL for the table
	var sqlFields = [
		'objectId VARCHAR(32) NOT NULL PRIMARY KEY',
		'created TIMESTAMP NOT NULL',
		'updated TIMESTAMP NOT NULL',
	];
	var sql = 'CREATE TABLE "' + meta.name + '" (' + sqlFields.join(', ') + ")";

	// create the table
	this.handle.run(sql, function(err) {
		if(err) {
			meta.fail(err);
		}
		else {
			meta.success();
		}
	});
}

module.exports = DbCoreService;
