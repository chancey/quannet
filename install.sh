NODE=node-v0.10.15-linux-x64
cd /opt
curl http://nodejs.org/dist/v0.10.15/$NODE.tar.gz > $NODE.tar.gz
tar zxf $NODE.tar.gz
cd $NODE

ln -s /opt/node-v0.10.15-linux-x64/bin/node /usr/bin/node
ln -s /opt/node-v0.10.15-linux-x64/bin/npm /usr/bin/npm
