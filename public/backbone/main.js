requirejs.config({
    // force a load order because we aren't using AMD compatible libraries
    shim: {
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: ['underscore', 'jquery', 'jqueryui'],
            exports: 'Backbone'
        },
        'bootstrap': {
            deps: ['jquery'],
            exports: 'bootstrap'
        }
    },
    
    // tell require where to look for files
    paths: {
        // libraries
        'jquery': '/js/lib/jquery-1.9.1',
        'jqueryui': '/js/lib/jquery-ui-1.10.3',
        'underscore': '/js/lib/underscore',
        'backbone': '/js/lib/backbone',
        'text': '/js/lib/text',
        'bootstrap': '/js/lib/bootstrap',
        
        // application
        'App': '/backbone/app',
        
        // router
        'Router': '/backbone/router',
        
        // models
        'DbModel': '/backbone/models/dbmodel',
        'SchemasModel': '/backbone/models/schemasmodel',
        'SchemaModel': '/backbone/models/schemamodel',
        
        // views
        'BrowseView': '/backbone/views/browseview',
        'HeaderView': '/backbone/views/headerview',
        'DesignView': '/backbone/views/designview'
    }
});

require(['App'], function(App, client) {
    App.initialize();
});
