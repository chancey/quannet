define('DbModel', [
    'jquery',
    'underscore',
    'backbone',
    'SchemasModel'
], function($, _, Backbone, SchemasModel) {
    var DbModel;

    DbModel = Backbone.Model.extend({
        defaults: {
            dbname: null,
            registered: false,
            description: null,
            schemas: {}
        },

        urlRoot: '/api/db',

        initialize: function() {
            this.schemas = new SchemasModel({
                dbname: this.dbname
            });
        }
    });

    return DbModel;
});
