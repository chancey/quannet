define('SchemasModel', [
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
    var SchemasModel;
    
    SchemasModel = Backbone.Model.extend({
        defaults: {
            dbname: null,
            schemas: {}
        },
    
        urlRoot: '/api/schemas',
    });
    
    return SchemasModel;
});
