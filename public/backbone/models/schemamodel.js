define('SchemaModel', [
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone) {
    var SchemaModel;
    
    SchemaModel = Backbone.Model.extend({
        defaults: {
            name: null
        },
    
        urlRoot: '/api/schema'
    });
    
    return SchemaModel;
});
