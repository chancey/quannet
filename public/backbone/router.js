define('Router', [
    'jquery',
    'underscore',
    'backbone',
    'HeaderView',
    'DesignView',
    'BrowseView'
], function($, _, Backbone, HeaderView, DesignView, BrowseView) {
    var AppRouter;

    AppRouter = Backbone.Router.extend({
        routes: {
            '': 'browse',
            'design': 'design',
            'browse': 'browse'
        },
        
        // called once upon instantiation.
        // setup the page locations we hook as well as the navigation bar
        initialize: function() {
            // cache the locations in the page where we may need to insert DOM elements
            this.elems = {
                'header': $('header'),
                'page-content': $('#page-content')
            };

            // insert the header/navbar
            this.headerView = new HeaderView();
            this.elems['header'].html(this.headerView.render().el);
        },

        design: function() {
            this.headerView.select('#design');

            var view = new DesignView();
            this.elems['page-content'].html(view.render().el);
        },
        
        browse: function() {
            this.headerView.select('#browse');
            
            var view = new BrowseView();
            this.elems['page-content'].html(view.render().el);
        }
    });

    return AppRouter;
});
