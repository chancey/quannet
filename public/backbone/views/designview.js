define('DesignView', [
  'jquery',
  'underscore',
  'backbone',
  'text!/backbone/templates/design.html',
  'text!/backbone/templates/design-welcome.html',
  'DbModel'
], function($, _, Backbone, Template, WelcomeTemplate, DbModel) {
    var DbView;
    
    DbView = Backbone.View.extend({
        // gives our div a class
        className: 'row',
    
        // initialization, called once when 'new' is called on our view
        initialize: function() {
            // generate the template
            this.template = _.template(Template);
      
            // create the model
            this.model = new DbModel({ name: "mydb" });
      
            // bind to change events on the model and render the view
            this.model.on('change', this.renderDbModel, this);
        },

        render: function() {
            // pass our model to the template for rendering
            $(this.el).html(this.template(this.model.toJSON()));

            $('#db-design-nav').accordion({
                heightStyle: "fill"
            });

            // tabs
            $('#db-design-tabs').tabs();
            var html = _.template(WelcomeTemplate, {});
            this.addTab('#db-design-tabs', 'welcome', 'Welcome', html);
            //$('#db-design-tabs').tabs({ active: 0 });
            $("div#db-design-tabs").tabs("refresh");
            
            this.model.schemas.on('change', this.renderSchemasModel, this);
            this.model.schemas.fetch();

            return this;
        },
    
        renderSchemasModel: function() {
            // tables
            var html = '';
            console.log(this.model.schemas);
            $.each(this.model.schemas, function(schemaName, schema) {
                //$.each(schema.tables, function(tableName, table) {
                    html += '<p>' + schemaName + '</p>';
                //});
            });
            console.log(html);
            $('#db-design-nav .tables').html(html);

            // views
            html = '<p>(no views)</p>';
            $('#db-design-nav .views').html(html);

            $('#db-design-nav').accordion("refresh");

            // return this to allow chaining
            return this;
        },

        addTab: function(tabs, tab_id, tab_title, content) {
            $("div" + tabs + " ul").append(
                "<li><a href='" + tab_id + "'>" + tab_title + "</a></li>"
            );
            $("div" + tabs).append(
                "<div id='" + tab_id + "'>" + content + "</div>"
            );
        },
    });
  
    return DbView;
});
