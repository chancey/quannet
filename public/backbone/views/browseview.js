define('BrowseView', [
  'jquery',
  'underscore',
  'backbone',
  'text!/backbone/templates/browse.html',
  'DbModel'
], function($, _, Backbone, Template, DbModel) {
    var DbView;
    
    DbView = Backbone.View.extend({
        // gives our div a class
        className: 'row',
    
        // initialization, called once when 'new' is called on our view
        initialize: function() {
            this.template = _.template(Template);
        },
    
        // handles rendering of the view
        render: function() {
            // pass our model to the template for rendering
            $(this.el).html(this.template());
      
            // return this to allow chaining
            return this;
        }
    });
  
    return DbView;
});
