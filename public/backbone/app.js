define('App', [
  'jquery',
  'underscore',
  'backbone',
  'Router'
], function($, _, Backbone, Router) {
    function initialize() {
        // start the path Router
        var app = new Router();
    
        // enable url # enabled history
        Backbone.history.start();
    }
  
    return {
        initialize: initialize
    };
});
