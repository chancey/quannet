##
## Continuous integration
##

ci: clean ci-pre test coverage doc

ci-pre:
	npm install

clean: clean-doc clean-coverage clean-instrument clean-test

##
## Unit tests
##

test: clean-instrument
	node_modules/yuitest/cli.js --format junitxml tests > build/junit.xml

clean-test:
	rm -rf build/junit.xml

##
## Code coverage
##

instrument:
	mkdir -p build/instrumented
	java -jar node_modules/yuitest-coverage/jar/yuitest-coverage.jar -d -o build/instrumented/services services

clean-instrument:
	rm -rf build/instrumented

coverage: instrument
	node tests/services/DbServiceTest.js > build/coverage.json
	java -jar node_modules/yuitest-coverage/jar/yuitest-coverage-report.jar -o build/coverage build/coverage.json

clean-coverage:
	rm -f build/coverage.json
	rm -rf build/coverage

##
## Documentation
##

doc:
	node_modules/yuidocjs/lib/cli.js controllers services

clean-doc:
	rm -rf doc
