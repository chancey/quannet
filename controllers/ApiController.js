var dbService = require("../services/dbservice");

/**
 * Web server controller for database operations.
 *
 * @namespace controllers
 * @class ApiController
 */
function ApiController() {
}

/**
 * Describe a database.
 *
 * @method DbDescribe
 * @param req {Object} Request callback.
 * @param res {Boolean} Result callback.
 */
ApiController.DbDescribe = function(req, res) {
    var db = dbService.getDatabase(req.param('dbname'));
    res.json(db.model);
}

/**
 * Create a database.
 *
 * @method DbCreate
 * @param req {Object} Request callback.
 * @param res {Boolean} Result callback.
 */
ApiController.DbCreate = function(req, res) {
    var result = dbService.createDatabase(req.param('name'));
    res.json(result.model);
}

/**
 * Get the tables for a database.
 *
 * @method DbTables
 * @param req {Object} Request callback.
 * @param res {Boolean} Result callback.
 */
ApiController.DbTables = function(req, res) {
    var db = dbService.getDatabase(req.param('dbname'));
    var schemas = db.getSchemas();
    res.json(schemas.model);
}

module.exports = ApiController;
