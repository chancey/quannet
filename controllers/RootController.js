/**
 * Default (home) controller.
 *
 * @namespace controllers
 * @class RootController
 */
function RootController() {
}

/**
 * Load application.
 *
 * @method View
 * @param req {Object} Request callback.
 * @param res {Boolean} Result callback.
 */
RootController.View = function(req, res) {
    res.render('app', { title: 'Quannet: ' + req.params.dbname });
};

module.exports = RootController;
